import numpy as np
import networkx as nx
import math

np.seterr(divide='ignore', invalid='ignore')

a = np.random.randint(2,size=[3,3])

n = np.size(a,1)

c = np.zeros((n,n))

i = 0
con = []
    
i = 0

while i < n:
    a[i,i] = 0
    n1 = a[:,i]
    n2 = np.transpose(a[i,:])
    n3 = n1+n2
    n4 = np.zeros((n,1))
    j = 0
    while j<=n-1:
        if n3[j]!=0:
            n4[j]=1
        j += 1
    con.append(sum(n4))
    i+=1
connectivity = np.mean(con)

b = nx.DiGraph(a)
        
print('Connectivity = ',connectivity)

degree_in = sum(a)
degree_out = sum(np.transpose(a))
d_dist = degree_out+degree_in
h = [degree_in,degree_out]

edges = sum(sum(a))

i=0
d = nx.floyd_warshall_numpy(b)
e = d
while i < n:
    j = 0
    while j < n:
        if e[i,j]<1:
            e[i,j] = math.inf
        j+=1
    i+=1

e = 1/e
eff = np.mean(np.transpose(e),0)
eff = eff*(n/(n-1))
efficiency = np.mean(eff)
print('Efficiency = ',efficiency) 

f = a+np.transpose(a)
dit = sum(f,0)
di = np.diag(np.matmul(a,a))
ff = np.matmul(f,f)
num = np.diag(np.matmul(ff,f))
den = 2*(dit*(dit-1)-2*di)
cc = np.divide(num,den)
cc = np.nan_to_num(cc)
clust = np.mean(cc)
print('Clustering Coefficient =', clust)

g = nx.transitive_closure(b)
r = nx.to_numpy_matrix(g)
reach = np.sum(r)
reachability = reach/(n**2-n)
print('Reachability =', reachability)

del b,c,i,n,n1,n2,n3,n4,j,con,d_dist,h,edges,d,e,eff,f,dit,di,ff,num,den,cc,g,r,reach