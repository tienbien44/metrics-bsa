# Metrics for Regulated Biochemical Systems

## Requirements

Python 3\
Numpy\
NetworkX

## Reference

Davis, J.D. and Voit, E.O., 2018. Metrics for Regulated Biochemical Pathway Systems. Bioinformatics.